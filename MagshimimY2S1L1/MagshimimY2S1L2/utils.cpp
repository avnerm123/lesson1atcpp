
#include "stack.h"
#include "utils.h"
#include <iostream>

int main()
{
	int* array = reverse10();
	for (int i = 0; i < SIZE; i++)
	{
		std::cout << array[i] << " ";
	}
}

void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	initStack(s);
	for (int i = 0; i < size; i++)
	{
		push(s, *(nums+i));
	}
	for (int i = 0; i < size; i++)
	{
		*(nums + i) = pop(s);
	}
}

int* reverse10()
{
	int* array;
	array = new int[SIZE];
	for (int i = 0; i < SIZE; i++)
	{
		std::cout << "Enter the " << i+1 << " Number in the array: ";
		std::cin >> array[i];
	}
	reverse(array, SIZE);
	return array;
}
