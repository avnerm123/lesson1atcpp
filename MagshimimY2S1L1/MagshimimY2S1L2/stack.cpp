#include <iostream>
#include "stack.h"



void push(stack* s, unsigned int element)
{
	linkedList** head_ref = &s->list;
	append(head_ref, element);
}

int pop(stack* s)
{
	linkedList** head_ref = &s->list;
	return deleteNode(head_ref);
}

void initStack(stack* s)
{
	linkedList** head_ref = &s->list;
	initlist(head_ref);
}

void cleanStack(stack* s)
{
	delete[] s->list;
	delete s;
}
