#define FIRST -293

typedef struct linkedList{
	int val;
	linkedList* next;
}linkedList;

void initlist(linkedList** list);
void append(linkedList** head_ref, int val);
int deleteNode(linkedList** head_ref);
void printList(linkedList* list);