#include "linkedList.h"
#include <iostream>


void initlist(linkedList** list)
{
	int val;
	val = 0;
	*list = new linkedList{val, NULL};
}


void append(linkedList** head_ref, int new_data)
{
	
	/* 1. allocate node */
	linkedList* new_node = new linkedList;
	

	linkedList* last = *head_ref;  /* used in step 5*/

	/* 2. put in the data  */


	new_node->val = new_data;

	/* 3. This new node is going to be the last node, so make next
		  of it as NULL*/
	new_node->next = NULL;

	/* 4. If the Linked List is empty, then make the new node as head */
	if (*head_ref == NULL)
	{
		*head_ref = new_node;
		return;
	}

	/* 5. Else traverse till the last node */
	while (last->next != NULL)
		last = last->next;

	/* 6. Change the next of last node */
	last->next = new_node;
	return;
}

int deleteNode(linkedList** head_ref)
{
	int popped = 0;
	// Store head node 
	linkedList* temp = *head_ref, * prev = new linkedList;

	// If head node itself holds the key to be deleted 
	if (temp->next == NULL)
	{
		
		*head_ref = temp->next;// Changed head 
		popped = temp->val;
		delete(temp);               // free old head 
		return popped;
	}

	// Search for the key to be deleted, keep track of the 
	// previous node as we need to change 'prev->next' 
	while (temp->next != NULL)
	{
		prev = temp;
		temp = temp->next;
	}

	
	
	popped = temp->val;
	// Unlink the node from linked list 
	prev->next = temp->next;
	
	delete(temp);  // Free memory 
	return popped;
}


void printList(linkedList* list)
{
	linkedList* temp = list;
	while (temp != NULL)
	{
		std::cout << "element: " << temp->val << std::endl;
		temp = temp->next;
	}
}