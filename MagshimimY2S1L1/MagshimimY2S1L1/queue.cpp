#include <iostream>
#include "queue.h"



int main()
{
	queue q;
	int size = 0, newVal = 0;
	std::cout << "Enter the size of the queue" << std::endl;
	std::cin >> size;
	initQueue(&q, size);
	for (int i = 0; i < size; i++)
	{
		std::cout << "Enter a value for the queue" << std::endl;
		std::cin >> newVal;
		enqueue(&q, newVal);
	}
	for (int i = 0; i < size; i++)
	{
		std::cout << "Elememt " << dequeue(&q) << " deleted!" << std::endl;
	}
	return 0;
}

void initQueue(queue* q, unsigned int size)
{
	q->elements = new int[size];
	q->capacity = size;
	q->front = 0;
	q->tail = 0;
	q->size = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->size == q->capacity)
	{
		std::cout << "overflow! queue is full" << std::endl;
		return;
	}
	q->elements[q->size] = newValue;
	q->size++;
	q->tail++;
}


int dequeue(queue* q)
{
	int item;
	if (q->size == 0)
	{
		return -1;
	}

	
	item = q->elements[q->front];
	q->elements[q->front] = 0;
	q->front++;

	return item;
}

void cleanQueue(queue* q)
{
	delete[] q->elements;
	delete q;
}